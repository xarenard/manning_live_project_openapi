package org.orquanet.controller;

import org.orquanet.entity.Transaction;
import org.orquanet.service.TransactionService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.Collection;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

    private TransactionService transactionService;

    public TransactionController(final TransactionService transactionService){
        this.transactionService = transactionService;
    }

    @RequestMapping(path = "/{accountNumber}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Transaction> findAllByAccountNumber(@PathParam("accountNumber") String accountNumber){
        return transactionService.findAllByAccountNumber();
    }
}
