package org.orquanet.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public final class Transaction {
    private String type;
    private String accountNumber;
    private String currency;
    private String amount;
    private String merchantName;
    private String merchantLogo;
}
