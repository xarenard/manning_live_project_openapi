package org.orquanet.service;

import org.orquanet.entity.Transaction;

import java.util.ArrayList;
import java.util.Collection;

public final class TransactionService {
    public Collection<Transaction> findAllByAccountNumber(){
        Collection<Transaction> transactions = new ArrayList<>();

        for(int i = 0;i < 4;i++){
            Transaction transaction = new Transaction();
            transaction.setType(String.format("type %d",i));
            transactions.add(transaction);
        }
        return transactions;
    }
}
