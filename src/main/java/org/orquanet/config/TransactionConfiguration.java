package org.orquanet.config;

import org.orquanet.service.TransactionService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TransactionConfiguration {

    @Bean
    public TransactionService transactionService(){
        return new TransactionService();
    }


}
