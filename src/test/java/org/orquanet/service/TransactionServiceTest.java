package org.orquanet.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TransactionServiceTest {

    @Autowired
    private TransactionService transactionService;

    @Test
    public void testFindAllByAccountNumber(){
        String accountNumber = "abcdef";
        Assertions.assertEquals(transactionService.findAllByAccountNumber().size(), 4);
    }
}
